//Variaveis globais
let dadosAPI;
let cart = [];
let cartTotal = 0;

//Chamada da API Marvel
$(document).ready(function(){
    const ApiKeyPublic = "4c5d319e9ac5fd63b5f8d957f49a4247";
    const ApiKeyPrivate = "bab6edbf927b7ba5186ef535041791f8df2cafac";
    const ts = "3627";
    const hash = "1a345510d34b4202ca860ad8d8240e6d";
    const noImg = 'https://baladasegura.rs.gov.br/themes/modelo-institucional/images/outros/GD_imgSemImagem.png';
    $.ajax({
        url: "https://gateway.marvel.com/v1/public/comics?ts=" + ts + "&apikey=" + ApiKeyPublic + "&hash=" + hash,
        type: "get",
        dataType: "json",
        success : function(data){
            console.log(data);
            for(var i = 0; i < data.data.count; i++){
                if(data.data.results[i].images[0] == null){
                    $("#destaque")
                    .append( "<div class='comics'><div class='foto'><img src='"+ 
                    noImg
                    +"' alt=''></div><div class='nome'><span>"
                     + data.data.results[i].series.name + 
                     "</span></div><div class='valor'>$ <span class='valor__val'>"
                     + data.data.results[i].prices[0].price +
                     "</span></div>" + 
                     "<button class='buy' onclick='abrirModalDesc(" + i + ")'>comprar</button>" 
                     + "</div>");                    
                } else{
                    $("#destaque")
                    .append( "<div class='comics'><div class='foto'><img src='"
                     + data.data.results[i].images[0].path + "." + data.data.results[i].images[0].extension + 
                    "' alt=''></div><div class='nome'><span>"
                     + data.data.results[i].series.name + 
                     "</span></div><div class='valor'>$ <span class='valor__val'>"
                     + data.data.results[i].prices[0].price +
                     "</span></div>" +
                     "<button class='buy' onclick='abrirModalDesc(" + i + ")'>comprar</button>" 
                     + "</div>");
                }
            }
            dadosAPI = data;
        },
        error : function(erro){
            console.log(erro);
        }
    });
});

//Funções do Cart
let addCart = (idProduct) => {
    var quantidade = $('#selectorQtd').val()
    var precoTotal = (dadosAPI.data.results[idProduct].prices[0].price * $('#selectorQtd').val()).toFixed(2)
    let product = {
        name: dadosAPI.data.results[idProduct].title,
        price: dadosAPI.data.results[idProduct].prices[0].price,
        serie: dadosAPI.data.results[idProduct].series.name,
        quantidade: parseFloat(quantidade),
        precoTotal: parseFloat(precoTotal),
    }
    if(dadosAPI.data.results[idProduct].images[0] == null){
        product.image = 'https://baladasegura.rs.gov.br/themes/modelo-institucional/images/outros/GD_imgSemImagem.png';
    } else{
        product.image = dadosAPI.data.results[idProduct].images[0].path + "." + dadosAPI.data.results[idProduct].images[0].extension;
    }
    cart.push(product);
    refreshCart();
    fecharDiv('.modal--descProduct')
    $('#selectorQtd').attr('value', 1)
    // alert("Produto " + product.name + " adicionado")
}

let refreshCart = () => {
    cartTotal = 0;
    $("#box-products").html("");
    for(var f = 0; f < cart.length; f++){
        var addProduct = `<div class="comicCart">
            <img src="${cart[f].image}" alt=""/>
            <button class="deleteProduct" onclick="deleteCart(${f})">X</button>
            <div class="comicCart__body">
                <span class="cartbody-nome">${cart[f].name}</span>
                <span class="cartbody-price">$ ${cart[f].precoTotal}</span>
            </div>
        </div>`
        $("#box-products")
        .append(addProduct)
        cartTotal = cartTotal + cart[f].precoTotal;
    }
    $('.box-total__Value').html(cartTotal.toFixed(2));
    $('.itensQtd span').html(cart.length);
}
let deleteCart = (idProduct) => {
    cart.splice(idProduct, 1)
    refreshCart()
}
let printCart = () => {
    console.log(cart);
}

//Funções modal Descrição produto
let abrirModalDesc = (idProduct) => {
    if(dadosAPI.data.results[idProduct].images[0] == null){
        $('.photoProduct img').attr('src', 'https://baladasegura.rs.gov.br/themes/modelo-institucional/images/outros/GD_imgSemImagem.png')
    } else{
        $('.photoProduct img').attr('src', dadosAPI.data.results[idProduct].images[0].path + "." + dadosAPI.data.results[idProduct].images[0].extension)
    }
    $('.dados--nome td').html(dadosAPI.data.results[idProduct].title)
    $('.dados--serie td').html(dadosAPI.data.results[idProduct].series.name)
    $('.dados--preco td').html(dadosAPI.data.results[idProduct].prices[0].price)
    $('.dados--maisdetalhes td a').attr('href', dadosAPI.data.results[idProduct].urls[0].url)
    $('.priceTotal span').html(dadosAPI.data.results[idProduct].prices[0].price)
    $('.qtdProduct .buy').attr('onclick', 'addCart(' + idProduct + ')')
    $('.modal--descProduct').css('display', 'flex')
}

//Funções Seletor de Quantidade
let aumentarValor = () => {
    var productQtd = $("#selectorQtd").val()
    var moreQtd = productQtd - 1 + 2
    var txtPrice = $('.dados--preco td').html()
    var calculoPrice = txtPrice * moreQtd
    $('#selectorQtd').attr('value', moreQtd)
    $('.priceTotal span').html(calculoPrice.toFixed(2))
}

let diminuirValor = () => {
    var productQtd = $("#selectorQtd").val()
    var lessQtd = productQtd - 1
    var txtPrice = $('.dados--preco td').html()
    var calculoPrice = txtPrice * lessQtd
    if(lessQtd < 0){
        $('#selectorQtd').attr('value', 0)
    } else {
        $('#selectorQtd').attr('value', lessQtd)
    }
    $('.priceTotal span').html(calculoPrice.toFixed(2))
}

//Funções para modais
let sanfona = (div) => {
    $(div).slideToggle('fast')
}
let fecharDiv = (div) => {
    $(div).css('display', 'none')
}